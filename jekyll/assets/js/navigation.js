window.onload = function() {
    var toggle = document.querySelector('#toggle');
    var menu = document.querySelector('#menu');
    var menuOpen = document.querySelector('#menu-open');
    var menuClose = document.querySelector('#menu-close');
    var menuItems = document.querySelectorAll('#menu li a');

    toggle.addEventListener('click', function(){
        if (menu.classList.contains('is-active')) {
            this.setAttribute('aria-expanded', 'false');
            menu.classList.remove('is-active');
            menuClose.style.display = "none";
            menuOpen.style.display = "inline-block";
        } else {
            menu.classList.add('is-active'); 
            this.setAttribute('aria-expanded', 'true');
            menuOpen.style.display = "none";
            menuClose.style.display = "inline-block";
        }
    });
}